using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class LevelTimer : MonoBehaviour {

    [field: Header("AutoAttach on Editor properties")]
    [field: SerializeField, ReadOnlyField] private Text timerText { get; set; }
    [field: SerializeField, ReadOnlyField] private Text timerTextHand { get; set; }
    [field: SerializeField] private bool revalidateProperties { get; set; }

    [field: Header("Timer properties")]
    [field: SerializeField, ReadOnlyField] private bool _isTimerStarted { get; set; }
    public bool IsTimerStarted {
        get { return _isTimerStarted; }
        set { _isTimerStarted = value; }
    }
    [field: SerializeField, ReadOnlyField] private float _timeElapsed { get; set; } // Tiempo transcurrido en el nivel
    public float TimeElapsed {
        get { return _timeElapsed; }
        set { _timeElapsed = value; }
    }
    private static string _timeFormatted { get; set; }
    public static string TimeFormatted {
        get { return _timeFormatted; }
        set { _timeFormatted = value; }
    }

    private string mins { get; set; }
    private string seconds { get; set; }

    void OnValidate() {
#if UNITY_EDITOR
        UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage/* && prefabConnected*/) {
            //Variables que solo se verificaran cuando estan en una escena
            if (timerText == null || revalidateProperties) {
                revalidateProperties = false;
                timerText = GameObject.Find("TimerText").GetComponent<Text>();
            }

            if (timerTextHand == null || revalidateProperties) {
                revalidateProperties = false;
                timerTextHand = GameObject.Find("HudRightHand").GetComponent<Text>();
            }
        }
#endif
    }

    void Update() {

        if (!IsTimerStarted)
            return;

        // El contador se detendra si el juego esta en pausa.
        if (!PauseManager.onPause) {
            _timeElapsed += Time.deltaTime;
            TimerTextUpdate();
        }
    }


    void TimerTextUpdate() {
        timerText.text = timerTextHand.text = _timeFormatted = FormatTime(_timeElapsed);
    }

    string FormatTime(float timeElapsed) {
        mins = Mathf.Floor(_timeElapsed / 60).ToString("00");
        seconds = (_timeElapsed % 60).ToString("00");

        return string.Format("{0}:{1}", mins, seconds);
    }

}