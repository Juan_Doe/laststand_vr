using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour {

    [field: Header("AutoAttach on Editor properties")]
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private PauseManager pauseManager { get; set; }
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private ScoreManager scoreManager { get; set; }

    [field: Header("Menu properties")]
    [field: SerializeField] private Text scorePauseText { get; set; }

    // From VR "real" class (ESI)
    [SerializeField] private GameObject handCanvas;
    [SerializeField] private InputActionProperty menuButton;
    [field: SerializeField] private InputActionProperty menuButtonSimulator { get; set; }

    private KeyCode leftHandSimulatorKey = KeyCode.LeftShift;

    void Update() {
        if (MainMenuManager.isMainMenuActive)
            return;

        if (menuButton.action.WasPressedThisFrame() || 
            (Input.GetKey(leftHandSimulatorKey) && menuButtonSimulator.action.WasPressedThisFrame())) {

            //handCanvas.SetActive(!handCanvas.activeSelf);
            pauseManager.Pause();

            if (handCanvas.activeInHierarchy)
                scorePauseText.text = string.Format("{0:D10}", scoreManager.CalculateScore(), 0);
        }
    }
}
