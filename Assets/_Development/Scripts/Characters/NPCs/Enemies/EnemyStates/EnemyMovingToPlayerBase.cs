﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyMovingToPlayerBase : EnemyState {
    public EnemyMovingToPlayerBase(EnemyZombie enemy, NavMeshAgent agent) : base(enemy, agent) {
        currentState = STATE.MovingToPlayerBase;

        agent.speed = enemy.MoveSpeed;
        agent.isStopped = false;
        agent.autoBraking = false;
    }

    public override void Enter() {
        // Used for sync agent with animation
        if (enemy.RootMotion)
            agent.updatePosition = false;

        base.Enter();
    }

    public override void Update() {
        base.Update();

        CheckersOnUpdate();

        //agent.speed *= enemy.Anim.GetCurrentAnimatorClipInfo(0)[0].clip.averageSpeed.sqrMagnitude;
        agent.SetDestination(enemy.PlayerBase.position);
        enemy.Anim.SetFloat("speed", agent.velocity.sqrMagnitude);

        //Debug.Log($"Sqr {agent.velocity.sqrMagnitude} vs Raiz {agent.velocity.magnitude}");
        //Debug.Log($"Anim Speed: {enemy.Anim.GetCurrentAnimatorClipInfo(0)[0].clip.averageSpeed.sqrMagnitude}");
    }

    public override void Exit() {
        agent.updatePosition = true;
        agent.ResetPath();

        base.Exit();
    }

    void CheckersOnUpdate() {
        if (CheckIfPlayerIsInRange()) {
            ChangeState(new EnemyMovingToPlayer(enemy, agent));
        }
        if (OnPlayerBase()) {
            ChangeState(new EnemyRandomWandering(enemy, agent));
        }
    }
}