using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEvent : MonoBehaviour {

    [field: Header("Autoattach properties")]
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private PlayerManager player { get; set; }

	[field: Header("--- Trigger Event properties ---")]
    [field: SerializeField] private EnemyZombie enemy { get; set; }
	[field: SerializeField, ReadOnlyField] private bool isTriggered { get; set; }

	[field: Header("--- Trigger Event Actions --- \n")]
	[field: SerializeField] private UnityEvent onTriggerEnter { get; set; }

	private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            player.TakeDamage(enemy.AttackDamage);
        }
    }
}