using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class LaserSwordController : MonoBehaviour {

    [SerializeField] private bool grab;
    [SerializeField] private InputActionProperty rightActionButton;
    [SerializeField] private InputActionProperty leftActionButton;

    [SerializeField] private HandDetector handDetector;

    [SerializeField] private GameObject laser;

    void Update() {
        if (rightActionButton.action.WasPressedThisFrame() && grab && handDetector.IsRightHand) {
            laser.SetActive(!laser.activeSelf);
        }
        else if (leftActionButton.action.WasPressedThisFrame() && grab && !handDetector.IsRightHand) {
            laser.SetActive(!laser.activeSelf);
        }
    }

    public void Agarre() {
        grab = !grab;
    }
}
