using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class W_Grenade : WeaponThrowable {

    [field: Header("Autoattach properties")]
    [field: SerializeField, GetComponent, ReadOnlyField] private Rigidbody rb { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private BoxCollider colBox { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private SphereCollider colSphe { get; set; }

    [field: Header("--- Grenade Properties ---")]
    [field: SerializeField] private LayerMask damageTargets { get; set; } = 1 << 7 | 1 << 10;   // 7 = Player, 10 = Enemy

    [field: Header("--- Grenade VFX ---")]
    [field: SerializeField] private ParticleSystem explosionVFX { get; set; }

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private bool ringPulled { get; set; }
    [field: SerializeField, ReadOnlyField] private float timer { get; set; }
    [field: SerializeField, ReadOnlyField] private bool thrown { get; set; }
    [field: SerializeField, ReadOnlyField] private bool exploded { get; set; }

    private Coroutine explodeCo { get; set; }

    void Start() {
        timer = weaponExplosionDelay;
    }

    void Update() {
        if (!ringPulled || exploded)
            return;

        // When ring is pulled, start timer.
        timer -= Time.deltaTime;

        if (timer <= 0f && !exploded) {
            exploded = true;
            explodeCo = StartCoroutine(ExplodeCo());
        }
    }

    public void PullRing() {
        if (ringPulled)
            return;

        ringPulled = true;
    }

    public void Throw() {
        if (thrown || !ringPulled)
            return;

        thrown = true;
        rb.isKinematic = false;
        rb.useGravity = true;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;

        //rb.AddForce(transform.forward * weaponThrowForce, ForceMode.Impulse);
        /*colBox.enabled = false;
        colSphe.enabled = true;*/
    }

    IEnumerator ExplodeCo() {
        /*
         * 1. Disable SkinnedMeshRenderer
         * 2. Get all enemies in radius
         * 3. Deal damage to all enemies in radius
         * 4. Add explosion force to all enemies in radius
         * 5. Destroy self
         */

        gameObject.transform.GetChild(1).gameObject.SetActive(false);
        
        ParticleSystem vfx = Instantiate(explosionVFX, transform.position, Quaternion.identity);
        vfx.gameObject.SetActive(true);
        Destroy(vfx.gameObject, 10f);

        Collider[] colliders = Physics.OverlapSphere(transform.position, weaponExplosionRadius, damageTargets);

        foreach (Collider nearbyObject in colliders) {
            if (nearbyObject.CompareTag("Enemy")) {
                EnemyZombie enemy = nearbyObject.GetComponent<EnemyZombie>();
                enemy.AffectedByExplosion = true;
                enemy.TakeDamage(weaponDamage);
                enemy.Rb.AddExplosionForce(weaponExplosionForce, transform.position, weaponExplosionRadius);
            }
            if (nearbyObject.CompareTag("Player")) {
                nearbyObject.GetComponent<PlayerManager>().TakeDamage(weaponDamage);
            }

            yield return null;  // For avoid lags
        }

        //yield return new WaitForSeconds(1f);

        //Destroy(gameObject);
        gameObject.SetActive(false);
    }

    private void ResetObject() {
        ringPulled = false;
        timer = weaponExplosionDelay;
        thrown = false;
        exploded = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
        gameObject.transform.GetChild(1).gameObject.SetActive(true);
        //explosionVFX.gameObject.SetActive(false);

        if (explodeCo != null)
            StopCoroutine(explodeCo);
    }

    private void OnDisable() {
        ResetObject();
    }

    private void OnDestroy() {
        if (gameObject == null)
            return;

        ResetObject();
    }
}