using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Rendering;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class PlayerManager : MonoBehaviour {

    [field: Header("Autoattach on Editor properties")]
    [field: SerializeField, GetComponent, ReadOnlyField] private CharacterController characterController { get; set; }
    [field: SerializeField, ReadOnlyField] public XRDirectInteractor rightHand { get; private set; }
    [field: SerializeField, ReadOnlyField] public XRDirectInteractor leftHand { get; private set; }
    [field: SerializeField] private bool revalidateProperties { get; set; }

    void OnValidate() {
#if UNITY_EDITOR
        UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage && prefabConnected) {
            if (revalidateProperties)
                ValidateAssings();
        }
#endif
    }

    void ValidateAssings() {
        // Get all the spawnPoints in the scene in a simplified way.
        if (rightHand == null || revalidateProperties) {
            rightHand = GameObject.Find("Right Direct Interactor").GetComponent<XRDirectInteractor>();
        }
        if (leftHand == null || revalidateProperties) {
            leftHand = GameObject.Find("Left Direct Interactor").GetComponent<XRDirectInteractor>();
        }
        revalidateProperties = false;
    }

    [field: Header("Player settings")]
    [field: Header("Health")]
    [field: SerializeField, ReadOnlyField] private float health { get; set; } = 100f;
    [field: SerializeField] private float maxHealth { get; set; } = 100f;
    [field: SerializeField] private Image healthBar { get; set; }

    //[field: Header("Damage fist")]


    [field: Header("Damage Screen Effects")]
    [field: SerializeField] private Volume damageVolume { get; set; }
    [field: SerializeField] private float damageVolumeDuration { get; set; } = 0.5f;
    private float damageVolDurAlt { get; set; }
    [field: SerializeField] private Volume polutionVolume { get; set; }
    [field: SerializeField] private float polutionVolumeVanishDuration { get; set; } = 0.5f;
    [field: SerializeField] private Volume lowHealthVolume { get; set; }
    [field: SerializeField] private float lowHealthVolumeDuration { get; set; } = 0.5f;

    [field: Header("Dead\n")]
    [field: SerializeField] private Transform initialPos { get; set; }
    [field: SerializeField] private UnityEvent onDead { get; set; }

    private bool isDead { get; set; }
    public bool IsDead { get => isDead; }

    public Transform playerTransform { get { return Camera.main.transform; } }

    private Coroutine activateDamageEffectCo { get; set; }
    private Coroutine lowHealthEffectCo { get; set; }
    private bool isPolluted { get; set; }
    private bool isLowHealth { get; set; }

    //private Vector3 initialPos;

    void Start() {
        health = maxHealth;
        damageVolDurAlt = damageVolumeDuration / 2f;

        //initialPos = transform.localPosition;
    }

    void Update() {
        /*Debug.Log($"Left hand holding: <color=green>{leftHand.hasSelection}</color>");
        Debug.Log($"Right hand holding: <color=green>{rightHand.hasSelection}</color>");*/

        if (isPolluted)
        {
            // Recibir da�o por contaminaci�n constante
            TakeDamage(3 * Time.deltaTime, isPolluted);
        }

        // If not has anything in hands and this is......
    }

    public void TakeDamage(float damage, bool polluted=false) {
        if (health > 0) {
            health -= damage;
            UpdateHealthBar();

            if (!polluted) {
                if (activateDamageEffectCo != null) {
                    StopCoroutine(activateDamageEffectCo);
                }
                activateDamageEffectCo = StartCoroutine(ActivateDamageEffectCo());
            }

            if (health < 15f && !isLowHealth) {
                isLowHealth = true;
                lowHealthEffectCo = StartCoroutine(ActivateLowHealthEffectCo());
            } else if (health > 15f && isLowHealth) {
                if (lowHealthEffectCo != null) {
                    StopCoroutine(lowHealthEffectCo);
                }
                lowHealthVolume.weight = 0f;
                isLowHealth = false;
            }

            if (health <= 0) {
                Die();
            }
        }

        Debug.Log($"Player health: <color=green>{health}</color>");
    }

    private void Die() {
        isDead = true;
        onDead.Invoke();
        StopAllCoroutines();
        // Reset volumes
        damageVolume.weight = 0f;
        polutionVolume.weight = 0f;
        lowHealthVolume.weight = 0f;

        characterController.enabled = false;
        transform.position = initialPos.position;
        transform.rotation = initialPos.rotation;
        characterController.enabled = true;
        PauseManager.SetOnPause(true);
    }

    // Activar el efecto de da�o
    private IEnumerator ActivateDamageEffectCo() {
        float startTime = Time.time;
        while (Time.time - startTime < damageVolumeDuration) {
            damageVolume.weight = (Time.time - startTime) / damageVolDurAlt;
            yield return null;
        }
        damageVolume.weight = 1f;
        yield return new WaitForSeconds(damageVolumeDuration);
        startTime = Time.time;
        while (Time.time - startTime < damageVolumeDuration) {
            damageVolume.weight = 1f - (Time.time - startTime) / damageVolumeDuration;
            yield return null;
        }
        damageVolume.weight = 0f;
    }

    // Activar el efecto de contaminaci�n
    public void ActivatePollutionEffect() {
        StartCoroutine(ActivatePollutionEffectCo());
    }

    private IEnumerator ActivatePollutionEffectCo() {
        isPolluted = true;
        while (polutionVolume.weight < 1f) {
            polutionVolume.weight += Time.deltaTime / polutionVolumeVanishDuration;
            yield return null;
        }
        polutionVolume.weight = 1f;
    }

    // Desactivar el efecto de contaminaci�n
    public void DeactivatePollutionEffect() {
        StartCoroutine(DeactivatePollutionEffectCo());
    }

    private IEnumerator DeactivatePollutionEffectCo() {
        while (polutionVolume.weight > 0f) {
            polutionVolume.weight -= Time.deltaTime / polutionVolumeVanishDuration;
            yield return null;
        }
        polutionVolume.weight = 0f;
        isPolluted = false;
    }

    // Activar el efecto de salud baja
    private IEnumerator ActivateLowHealthEffectCo() {
        while (lowHealthVolume.weight < 1f) {
            lowHealthVolume.weight += Time.deltaTime / lowHealthVolumeDuration;
            yield return null;
        }
        lowHealthVolume.weight = 1f;
    }

    private void UpdateHealthBar() {
        healthBar.fillAmount = health / maxHealth;
    }
}