using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class XRFilterSocketInteractor : XRSocketInteractor {
    // XRSnapInteractor is an XRSocketInteractor that filters socketable items by tag
    [field: Header("Filter PARAMS")]
    [field: SerializeField] protected bool doFilterByTag { get; set;}
    [field: SerializeField, TagSelector] protected string filterTag { get; set; }
    //[SerializeField] protected string[] filterTags;


    public override bool CanHover(IXRHoverInteractable interactable) {
        if (doFilterByTag && interactable is MonoBehaviour gameObject) {
            /*foreach (string filterTag in filterTags) {
                if (!gameObject.CompareTag(filterTag)) {
                    return false;
                }
            }*/
            if (!gameObject.CompareTag(filterTag)) {
                return false;
            }
        }
        return base.CanHover(interactable);
    }

    public override bool CanSelect(IXRSelectInteractable interactable) {
        if (doFilterByTag && interactable is MonoBehaviour gameObject) {
            /*foreach (string filterTag in filterTags) {
                if (!gameObject.CompareTag(filterTag)) {
                    return false;
                }
            }*/
            if (!gameObject.CompareTag(filterTag)) {
                return false;
            }
        }
        return base.CanSelect(interactable);
    }
}