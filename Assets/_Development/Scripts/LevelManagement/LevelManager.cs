using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	[field: Header("AutoAttach on Editor properties")]
	[field: SerializeField, GetComponent, ReadOnlyField] private LevelTimer levelTimer { get; set; }    // Manages the level timer.
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private RespawnManager respawnManager { get; set; }    // Manages the respawn
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private EnemyManager enemyManager { get; set; }    // Manages the enemies

    [field: Header("--- Level manager properties ---")]
    [field: SerializeField] private float enemyWaveDurationMins { get; set; } = 8f;
    [field: SerializeField] private float enemyWaveIntervalSenconds { get; set; } = 15f;

    private float enemyWaveDurationSecs { get; set; }
    private bool isWaveActive { get; set; }
    private float currentTimeElapsed { get; set; }

    private int currentWave { get; set; } = 0;
    public int CurrentWave { get => currentWave; }

    public void StartApocalypse() {
        levelTimer.IsTimerStarted = true;
    }

	public void StopApocalypse() {
        levelTimer.IsTimerStarted = false;
    }

    void Start() {
        enemyWaveDurationSecs = enemyWaveDurationMins * 60;

        //StartApocalypse();
    }

    void Update() {
        // If the level timer is not started, do nothing.
        if (!levelTimer.IsTimerStarted || PauseManager.onPause)
            return;

        if (isWaveActive) {
            // Update the time elapsed
            currentTimeElapsed += Time.deltaTime;

            // If the time elapsed is greater than or equal to the wave duration, end the wave.
            if (currentTimeElapsed >= enemyWaveDurationSecs) {
                EndCurrentWave();
            }
        }
        else {
            // Update the time elapsed
            currentTimeElapsed += Time.deltaTime;

            // Check if the time elapsed is greater than or equal to the wave interval, start a new wave.
            if (currentTimeElapsed >= enemyWaveIntervalSenconds) {
                StartNewWave();
            }
        }
    }

    void StartNewWave() {

        isWaveActive = true;
        currentTimeElapsed = 0f;
        currentWave++;

        //Debug.Log($"Wave <color=red>{currentWave}</color> started.");

        enemyManager.StartEnemyRespawn();
    }

    void EndCurrentWave() {
        //Debug.Log($"Wave Ended.");

        isWaveActive = false;
        currentTimeElapsed = 0f;

        enemyManager.StopEnemyRespawn();
        respawnManager.ReactivateAmmo();
        respawnManager.ReactivateWeapons();
    }

    public void EndLevel() {
        StopApocalypse();
        enemyManager.StopWaves();
    }
}