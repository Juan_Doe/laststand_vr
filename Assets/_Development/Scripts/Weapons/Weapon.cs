using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Weapon : MonoBehaviour {

	protected enum WeaponType {
		NONE,
        MELEE,
        RANGED,
		THROWABLE
    }
	protected enum MeleeType {
        SLASH,
		BLUNT
    }
	protected enum RangedType {
        PISTOL,
		SHOTGUN,
		AUTOMATIC,
		RPG
	}
	protected enum ThrowableType {
        EXPLOSIVE_THROWABLE,
		EXPLOSIVE_REMOTE,
		SLASH_THROWABLE
    }

	[field: Header("--- Weapon Properties ---")]
	[field: SerializeField] protected string weaponName { get; set; }
	[field: SerializeField] protected float weaponDamage { get; set; } = 10f;
	public float WeaponDamage { get { return weaponDamage; } }

    [field: Header("Debug")]
	[field: SerializeField, ReadOnlyField] protected bool isSelected { get; set; }
	public bool IsSelected { get { return isSelected; } }

    public void WeaponSelected(bool isSelected) {
        if (isSelected)
            this.isSelected = true;
        else
            this.isSelected = false;
    }
}