using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PoolItem {
	
	[field: SerializeField] public GameObject Prefab { get; set; }
	[field: SerializeField] public int PoolCount { get; set; } = PoolManager.DEFAULT_POOL_COUNT;
}