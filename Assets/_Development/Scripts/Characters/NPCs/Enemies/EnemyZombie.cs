using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class EnemyZombie : MonoBehaviour {

    [field: Header("Autoattach properties")]
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private EnemyManager enemies { get; set; }
    public EnemyManager Enemies { get => enemies; }
    [field: SerializeField, GetComponent, ReadOnlyField] private NavMeshAgent agent { get; set; }
    [field: SerializeField, ReadOnlyField] private Transform playerBase { get; set; }
    public Transform PlayerBase { get => playerBase; }
    [field: SerializeField, GetComponent, ReadOnlyField] private Animator anim { get; set; }
    public Animator Anim { get => anim; }
    [field: SerializeField, GetComponent, ReadOnlyField] private Rigidbody rb { get; set; }
    public Rigidbody Rb { get => rb; }
    [field: SerializeField, GetComponent, ReadOnlyField] private Collider col { get; set; }
    public Collider Col { get => col; }
    [field: SerializeField] private bool revalidateProperties { get; set; } = false;

    [field: Header("Move settings")]
    [field: SerializeField] private float moveSpeed { get; set; } = 4f;
    public float MoveSpeed { get => moveSpeed; }
    [field: SerializeField] private bool rootMotion { get; set; }
    public bool RootMotion { get => rootMotion; }

    [field: Header("Attack settings")]
    [field: SerializeField] private BoxCollider attackCollider { get; set; }
    [field: SerializeField] private float attackDamage { get; set; } = 5f;
    public float AttackDamage { get => attackDamage; }
    [field: SerializeField] private LayerMask targetMasks { get; set; }
    [field: SerializeField] private float distanceDetection { get; set; } = 5f;
    public float DistanceDetection { get => distanceDetection; }
    [field: SerializeField] protected float attackCooldown { get; set; } = 1f;
    public float AttackCooldown { get => attackCooldown; }
    private float attackTimer { get; set; } = 0f;
    public float AttackTimer { get => attackTimer; }

    [field: Header("Health settings")]
    [field: SerializeField] private float maxHealth { get; set; } = 20f;
    [field: SerializeField, ReadOnlyField] private float health { get; set; } = 20f;
    public float CurrentHealth { get => health; }

    [field: Header("Dead Event \n")]
    [field: SerializeField] private UnityEvent onDead { get; set; }

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private bool isDead { get; set; }
    public bool IsDead { get => isDead; set => isDead = value; }
    [field: SerializeField, ReadOnlyField] private bool affectedByExplosion { get; set; }
    public bool AffectedByExplosion { get => affectedByExplosion; set => affectedByExplosion = value; }
    [field: SerializeField, ReadOnlyField] private bool inPlayerBase { get; set; }
    public bool InPlayerBase { get => inPlayerBase; }
    [field: SerializeField, ReadOnlyField] public bool isAttacking { get; set; }

    private EnemyState currentState { get; set; }

    void OnValidate() {
#if UNITY_EDITOR
        UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage && prefabConnected) {
            // Variables that will only be checked when they are in a scene
            if (!Application.isPlaying) {
                if (playerBase == null || revalidateProperties) {
                    revalidateProperties = false;
                    playerBase = GameObject.FindGameObjectWithTag("PlayerBase").transform;
                }
            }
        }

#endif
    }

    void Start() {
        if (anim != null) {
            anim.applyRootMotion = rootMotion;
        }

        if (attackCollider != null) {
            attackCollider.enabled = false;
        }

        currentState = new EnemyMovingToPlayerBase(this, agent);
    }

    void Update() {
        if (enemies.WaveState == EnemyManager.WaveStates.Stopped && !isDead)
            Die();

        currentState = currentState.Process();

        if (!isDead) {
            // Attack cooldown
            if (attackTimer > 0) {
                attackTimer -= Time.deltaTime;
            }
        }

        //Debug.Log($"<color=blue>Animator: {anim.GetCurrentAnimatorClipInfo(0)[0].clip.name}</color>");
    }

    public void TakeDamage(float damage) {
        if (health > 0) {
            health -= damage;
            if (health <= 0) {
                health = 0;
                Die();
            }
        }
    }

    protected void Die() {
        isDead = true;
        //gameObject.SetActive(false);

        // For not counting the enemy as defeated when it dies by the end of game.
        if (!(enemies.WaveState == EnemyManager.WaveStates.Stopped)) {
            onDead.Invoke();
        }
    }

    void OnCollisionEnter(Collision collision) {
        if (isDead)
            return;

        if (collision.gameObject.CompareTag("Player") && attackTimer <= 0 && !isAttacking) {
            attackTimer = attackCooldown;
            currentState.ChangeState(new EnemyAttacking(this, agent));
        }
    }

    void OnTriggerEnter(Collider other) {
        if (isDead)
            return;

        if (other.gameObject.CompareTag("PlayerBase")) {
            inPlayerBase = true;
        }
    
    }

    void OnTriggerExit(Collider other) {
        if (isDead)
            return;

        if (other.gameObject.CompareTag("PlayerBase")) {
            inPlayerBase = false;
        }
    }

    // Used for syncing the navmesh agent with the animation
    void OnAnimatorMove() {
        if (!agent.updatePosition) {
            Vector3 position = anim.rootPosition;
            position.y = agent.nextPosition.y;
            transform.position = position;
            agent.nextPosition = transform.position;

            // Fix rigidbody physics
            //rb.velocity = new Vector3(anim.deltaPosition.x * 60, rb.velocity.y, anim.deltaPosition.z * 60);
            //rb.velocity = anim.deltaPosition / Time.deltaTime;
        }
    }

    public void EnableTriggerAttack() {
        isAttacking = true;
        attackCollider.enabled = true;
    }

    public void DisableTriggerAttack() {
        isAttacking = false;
        attackCollider.enabled = false;
    }

    public void ResetEnemy1() {
        isDead = false;
        health = maxHealth;
        attackTimer = 0f;
        isAttacking = false;
        attackCollider.enabled = false;
        affectedByExplosion = false;
        inPlayerBase = false;

        // Reset animator
        anim.Rebind();
        anim.Update(0f);
    }

    public void ResetEnemy2() {
        agent.enabled = true;
        currentState?.ChangeState(new EnemyMovingToPlayerBase(this, agent));
    }

    void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, distanceDetection);
    }

    /*void OnDisable() {
        //ResetEnemy();

    }*/
}