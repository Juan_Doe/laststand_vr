using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class PoolManager : MonoBehaviour {

    // Default pool size for each pool item
    public static int DEFAULT_POOL_COUNT = 1;

    // Data Setup
    [field: SerializeField] private List<PoolItem> poolableObjects { get; set; } = new List<PoolItem>();

    [field: SerializeField, Tooltip("Collection checks will throw errors if we try to release an item that is already in the pool.")] 
    private bool collectionChecks { get; set; } = true;

    // Maximum collection size allocated per pooled object
    [field: SerializeField] private int maxPoolSize { get; set; } = 10;

    private Dictionary<string, IObjectPool<PooledGameObject>> pool { get; set; }
    public Dictionary<string, IObjectPool<PooledGameObject>> Pool {
        get {
            return pool;
        }
    }

    // Pool is ready to be used
    public bool IsReady { get; private set; }

    public Action OnCleanup;
    public Action OnRelease;

    // --------- Custom ------------

    public enum ObjectType {
        Undefined,
        Clip,
        Weapon,
        Throwable,
        Enemy,
        VFX
    }

    private string prefabNullError = "Pool Item prefab is null";

    public PooledGameObject Spawn(string id, Transform parent = null) {
        if (pool.TryGetValue(id, out var objectPool)) {
            var pooledGO = objectPool.Get();
            if (parent) {
                pooledGO.transform.SetParent(parent);
            }

            return pooledGO;
        }

        return null;
    }

    public void InstantiatePool() {
        if (IsReady) {
            return;
        }

        pool = DictionaryPool<string, IObjectPool<PooledGameObject>>.Get();

        // Memory allocation and initialize GameObject
        poolableObjects.ForEach(
            item => {
                // Create stack in dictionary
                CreateObjectsPool(item);
                InitializeObjectsPool(item, item.PoolCount);
            }
        );

        // Disable all pooled objects
        OnRelease?.Invoke();
        IsReady = true;
    }

    public void CleanupPool(bool garbageCollect = true) {
        if (!IsReady) {
            return;
        }

        // Destroy all game objects and release allocated memory
        OnCleanup?.Invoke();
        DictionaryPool<string, IObjectPool<PooledGameObject>>.Release(pool);

        IsReady = false;
        OnCleanup = null;

        if (garbageCollect) {
            GC.Collect();
        }
    }

    public void ReleasePooledObject() {
        if (!IsReady) {
            return;
        }

        OnRelease?.Invoke();
    }

    private void CreateObjectsPool(PoolItem item) {
        if (!item.Prefab) {
            throw new Exception(prefabNullError);
        }

        var id = item.Prefab.name;

        // Game object doesn't get created at this point, we need to call pool.Get() to do so
        pool.Add(
            id,
            new ObjectPool<PooledGameObject>(
                () => CreatePooledItem(item.Prefab),
                OnGetFromPool,
                OnReleaseToPool,
                OnDestroyPoolObject,
                collectionChecks,
                item.PoolCount,
                maxPoolSize
            )
        );
    }

    private void InitializeObjectsPool(PoolItem item, int count) {
        if (!item.Prefab) {
            throw new Exception(prefabNullError);
        }

        var id = item.Prefab.name;
        if (!pool.TryGetValue(id, out var objectPool)) {
            CreateObjectsPool(item);
            objectPool = pool[id];
        }

        // Object shouldn't be release at this point else it will just take the same item from the top of the stack
        for (var i = 0; i < count; ++i) {
            objectPool.Get();
        }
    }

    private PooledGameObject CreatePooledItem(GameObject prefab) {
        GameObject go = Instantiate(prefab, Vector3.zero, Quaternion.identity, transform);
        go.name = prefab.name;

        var pooledGO = go.AddComponent<PooledGameObject>();
        pooledGO.Initialize(this, prefab.name);
        return pooledGO;
    }

    private void OnGetFromPool(PooledGameObject pooledGO) {
        pooledGO.SetActive(true);
    }

    private void OnReleaseToPool(PooledGameObject releaseGO) {
        releaseGO.transform.SetParent(transform);
        releaseGO.SetActive(false);
    }

    private void OnDestroyPoolObject(PooledGameObject destroyedGO) {
        Destroy(destroyedGO.gameObject);
    }

}