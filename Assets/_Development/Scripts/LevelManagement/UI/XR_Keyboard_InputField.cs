using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VrKeyboard;

public class XR_Keyboard_InputField : InputField {

    public Keyboard keyboard;

    protected override void Start() {
        base.Start();
        if (keyboard == null) {
            keyboard = transform.GetChild(2).GetComponent<Keyboard>();
        }
    }

    private void Update() {
        if (isFocused) {
            if (keyboard != null) {
                keyboard.KeyPressed.RemoveAllListeners();
                keyboard.EnterPressed.RemoveAllListeners();
                keyboard.BackspacePressed.RemoveAllListeners();

                keyboard.KeyPressed.AddListener(onKey);
                keyboard.EnterPressed.AddListener(onEnter);
                keyboard.BackspacePressed.AddListener(onBackspace);
            }
        }
    }

    protected void onKey(string s) {
        if (selectionAnchorPosition == selectionFocusPosition) {
            text = text.Insert(caretPosition, s);
            caretPosition += 1;
        }
        else {
            int start = Mathf.Min(selectionAnchorPosition, selectionFocusPosition);
            int end = Mathf.Max(selectionAnchorPosition, selectionFocusPosition);
            text = text.Remove(start, end - start).Insert(start, s);
        }
    }

    protected void onEnter() {
    }

    protected void onBackspace() {
        if (selectionAnchorPosition == selectionFocusPosition) {
            caretPosition -= 1;
            /*
             * Fix:
             * ArgumentOutOfRangeException: Index and count must refer to a location within the string. Parameter name: count
             */
            if (caretPosition < 0 || caretPosition >= text.Length) {
                return;
            }

            text = text.Remove(caretPosition, 1);
        }
        else {
            /*
             * Fix:
             * ArgumentOutOfRangeException: Index and count must refer to a location within the string. Parameter name: count
             */
            if (selectionAnchorPosition < 0 || selectionFocusPosition < 0) {
                return;
            }

            int start = Mathf.Min(selectionAnchorPosition, selectionFocusPosition);
            int end = Mathf.Max(selectionAnchorPosition, selectionFocusPosition);
            text = text.Remove(start, end - start);
            caretPosition = start;
        }

    }
}