using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Projectile", menuName = "ScriptableObject/Weapons/Ammo/Projectile")]
public class ProjectileSO : ScriptableObject {
    public enum ProjectileType {
        Bullet,
        Pellet,
        Rocket,
        Grenade,
        Shuriken,
        Kunai
    }

    [field: Header("--- Projectile Properties ---")]
    [field: SerializeField] public ProjectileType projectileType { get; private set; } = ProjectileType.Bullet;
    [field: SerializeField] public float projectileDamage { get; private set; } = 10f;
    [field: SerializeField] public float projectileSpeed { get; private set; } = 10f;
    [field: SerializeField] public float projectileLifetime { get; private set; } = 100f;
}