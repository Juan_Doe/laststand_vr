using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponMelee : Weapon {

	[field: Header("Melee Properties")]
    [field: SerializeField, ReadOnlyField] protected WeaponType weaponType { get; set; } = WeaponType.MELEE;
    [field: SerializeField] protected MeleeType meleeType { get; set; } = MeleeType.SLASH;


}