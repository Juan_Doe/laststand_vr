﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyMovingToPlayer : EnemyState {

    private float pursuitTimer = 0f;
    private float pursuitTimerMax = 5f;

    private Transform playerTransform;

    public EnemyMovingToPlayer(EnemyZombie enemy, NavMeshAgent agent) : base(enemy, agent) {
        currentState = STATE.MovingToPlayer;

        agent.speed = enemy.MoveSpeed;
        agent.isStopped = false;
        agent.autoBraking = false;
    }
    public override void Enter() {
        if (enemy.RootMotion)
            agent.updatePosition = false;

        playerTransform = enemy.Enemies.Player.playerTransform;
        pursuitTimer = 0f;

        base.Enter();
    }

    public override void Update() {
        base.Update();

        CheckersOnUpdate();

        agent.SetDestination(playerTransform.position);
        enemy.Anim.SetFloat("speed", agent.velocity.sqrMagnitude);
    }

    public override void Exit() {
        agent.updatePosition = true;
        agent.ResetPath();

        base.Exit();
    }

    void CheckersOnUpdate() {
        if (!CheckIfPlayerIsInRange()) {
            // Start timer
            pursuitTimer += Time.deltaTime;

            // If timer is greater than 5 seconds, change state to MovingToPlayerBase
            if (pursuitTimer >= pursuitTimerMax) {
                ChangeState(new EnemyMovingToPlayerBase(enemy, agent));
            }

        }
    }

}