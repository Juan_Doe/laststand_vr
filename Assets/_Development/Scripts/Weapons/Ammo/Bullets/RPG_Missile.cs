using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class RPG_Missile : Projectile {

    [field: Header("__ Autottach properties __")]
    [field: SerializeField, GetComponent, ReadOnlyField] private Rigidbody rb { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private Collider col { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private XRGrabInteractable myInteractable { get; set; }

    [field: Header("--- RPG Misile Properties ---")]
    [field: SerializeField] private LayerMask damageTargets { get; set; } = 1 << 7 | 1 << 10;   // 7 = Player, 10 = Enemy
    [field: SerializeField] private float weaponExplosionRadius { get; set; } = 5f;
    [field: SerializeField] private float weaponExplosionForce { get; set; } = 100f;
    [field: SerializeField] private float weaponExplosionDelay { get; set; } = 0f;

    [field: Header("--- Projectile VFX ---")]
    [field: SerializeField] private ParticleSystem propulsionVFX { get; set; }
    [field: SerializeField] private ParticleSystem explosionVFX { get; set; }


    private float onCollisionDamage = 0f;
    private Coroutine explodeCo { get; set; }
    private bool launched { get; set; }
    private bool exploded { get; set; }

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private InteractionLayerMask previousLayerMask { get; set; }
    [field: SerializeField, ReadOnlyField] private InteractionLayerMask temporalLayerMask { get; set; } = 2;    // 2 = Direct Drag

    void Start() {
        onCollisionDamage = projectileData.projectileDamage / 2f;

        previousLayerMask = myInteractable.interactionLayers;
    }

    void OnTriggerEnter(Collider other) {
        if (!launched || exploded)
            return;

        if (other.gameObject.CompareTag("Enemy")) {
            other.gameObject.GetComponent<EnemyZombie>().TakeDamage(onCollisionDamage);
        }

        exploded = true;
        Debug.Log($"<color=red>Trigger: {other.name}</color>");
        Explode();
    }

    public void Disparar() {
        gameObject.layer = 11;  // 11 = Projectile
        myInteractable.interactionLayers = temporalLayerMask;

        StartCoroutine(ShootCo());
    }

    void Explode() {
        /*
         * 0. Stop moving and disable child object.
         * 1. Start explosion coroutine
         */

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        rb.isKinematic = true;
        gameObject.transform.GetChild(0).gameObject.SetActive(false);

        ParticleSystem vfx = Instantiate(explosionVFX, transform.position, Quaternion.identity);
        vfx.gameObject.SetActive(true);
        Destroy(vfx.gameObject, 10f);

        explodeCo = StartCoroutine(ExplodeCo());
    }

    private void ResetObject() {
        if (explodeCo != null)
            StopCoroutine(explodeCo);

        if (!gameObject.transform.GetChild(0).gameObject.activeInHierarchy) {
            gameObject.transform.GetChild(0).gameObject.SetActive(true);
        }

        col.isTrigger = false;
        rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
        rb.useGravity = true;
        rb.isKinematic = false;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;
        gameObject.layer = 8;   // 8 = Direct Drag
        myInteractable.interactionLayers = previousLayerMask;
        propulsionVFX.gameObject.SetActive(false);
    }

    void OnDisable() {
        ResetObject();
    }

    void OnDestroy() {
        if (gameObject == null)
            return;

        ResetObject();
    }

    IEnumerator ShootCo() {
        yield return null;  // Skip the ****ing frame that reenable the rigidbody gravity and kill the AddFroce of previous frame

        //Debug.Break();

        if (col != null)
            col.isTrigger = true;

        rb.useGravity = false;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

        launched = true;

        rb.AddForce(transform.forward * projectileData.projectileSpeed, ForceMode.Impulse);
        propulsionVFX.gameObject.SetActive(true);

        //Destroy(gameObject, projectileData.projectileRange);
        yield return new WaitForSeconds(projectileData.projectileLifetime);
        gameObject.SetActive(false);
    }

    IEnumerator ExplodeCo() {
        /*
         * 1. Get all enemies in radius
         * 2. Deal damage to all enemies in radius
         * 3. Add explosion force to all enemies in radius
         * 4. Destroy self
         */

        Collider[] colliders = Physics.OverlapSphere(transform.position, weaponExplosionRadius, damageTargets);

        foreach (Collider nearbyObject in colliders) {
            if (nearbyObject.CompareTag("Enemy")) {
                EnemyZombie enemy = nearbyObject.GetComponent<EnemyZombie>();
                enemy.AffectedByExplosion = true;
                enemy.TakeDamage(projectileData.projectileDamage);
                enemy.Rb.AddExplosionForce(weaponExplosionForce, transform.position, weaponExplosionRadius);
            }
            if (nearbyObject.CompareTag("Player")) {
                nearbyObject.GetComponent<PlayerManager>().TakeDamage(projectileData.projectileDamage);
            }

            yield return null;  // For avoid lags
        }

        yield return null;

        //Destroy(gameObject);
        gameObject.SetActive(false);
    }
}