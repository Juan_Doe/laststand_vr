using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponThrowable : Weapon {

	[field: Header("Throwable Properties")]
	[field: SerializeField, ReadOnlyField] protected WeaponType weaponType { get; set; } = WeaponType.THROWABLE;
    [field: SerializeField] protected ThrowableType throwableType { get; set; } = ThrowableType.EXPLOSIVE_THROWABLE;

	[field: SerializeField] protected float weaponThrowForce { get; set; } = 100f;
	[field: SerializeField] protected float weaponExplosionRadius { get; set; } = 5f;
	[field: SerializeField] protected float weaponExplosionForce { get; set; } = 100f;
	[field: SerializeField] protected float weaponExplosionDelay { get; set; } = 3f;

}