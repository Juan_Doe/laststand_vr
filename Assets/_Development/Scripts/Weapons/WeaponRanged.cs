using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class WeaponRanged : Weapon {

	[field: Header("--- Ranged Properties ---")]
    [field: SerializeField, ReadOnlyField] protected WeaponType weaponType { get; set; } = WeaponType.RANGED;

    [field: SerializeField] protected RangedType rangedType { get; set; } = RangedType.PISTOL;

    [field: SerializeField] protected float weaponRange { get; set; } = 100f;
    [field: SerializeField] protected float weaponFireRate { get; set; } = 15f;
    protected float nextTimeToFire { get; set; } = 0f;
    [field: SerializeField] protected Poolable damageVFXprefab { get; set; }
    protected ObjectPool damageVFXPool { get; set; }
    [field: SerializeField] protected ParticleSystem muzzleFlash { get; set; }

    void Start() {
        if (damageVFXPool == null)
            damageVFXPool = ObjectPool.CreatePool(damageVFXprefab, 12, $"__Pool_[{rangedType}]__BloodVFXPool__");
    }
}