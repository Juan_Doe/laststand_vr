﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyAttacking : EnemyState {

    private bool attackPlaying { get; set; }

    public EnemyAttacking(EnemyZombie enemy, NavMeshAgent agent) : base(enemy, agent) {
        currentState = STATE.Attacking;

        agent.isStopped = true;
    }

    public override void Enter() {
        // TODO: Play attack animation
        enemy.Anim.SetTrigger("Attack");

        // ToRemove: Damage player
        //enemy.Enemies.Player.TakeDamage(enemy.AttackDamage);

        enemy.Rb.drag = 100f;
        enemy.Rb.velocity = Vector3.zero;
        enemy.Rb.isKinematic = true;

        base.Enter();
    }

    public override void Update() {
        base.Update();

        // Get the name of the current animation
        //Debug.Log($"<color=yellow>Animator: {enemy.Anim.GetCurrentAnimatorClipInfo(0)[0].clip.name}</color>");

        /*
         * 1. Espera a que la animación de ataque empiece a ejecutarse.
         * 2. Cuando la animación de ataque se esté ejecutando, espera a que termine.
         * 
         * Todo esto para evitar el bug por el cual el enemigo se desplaza hacia atrás cuando el jugador embiste al enemigo en el proceso.
         */
        if (enemy.Anim.GetCurrentAnimatorClipInfo(0)[0].clip.name.Equals("Attack") || attackPlaying) {
            attackPlaying = true;
            if (!enemy.Anim.GetCurrentAnimatorStateInfo(0).IsTag("Attacking")) {
                ChangeState(new EnemyMovingToPlayer(enemy, agent));
            }
        }
        
    }

    public override void Exit() {
        enemy.DisableTriggerAttack();

        agent.ResetPath();
        enemy.Rb.isKinematic = false;
        enemy.Rb.velocity = Vector3.zero;
        enemy.Rb.drag = 0f;

        //Debug.Log($"EnemyAttacking: {stage} -> {nextState}");

        base.Exit();
    }
}