using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class AmmoClip : MonoBehaviour {

    [field: Header("--- Autoattach Properties ---")]
    [field: SerializeField, GetComponent, ReadOnlyField] private XRGrabInteractable myInteractable { get; set; }

    [field: Header("--- Ammo Clip Properties ---")]
    [field: SerializeField] private int maxAmmo { get; set; } = 12;
    [field: SerializeField, ReadOnlyField] private int ammoCount { get; set; } = 12;
    public int AmmoCount { get => ammoCount; }

    [field: Header("--- For Specific Cases ---")]
    [field: Header("--- Colliders overlap fix ---")]
    [field: SerializeField] private Collider defaultCol { get; set; }
    [field: SerializeField] private Collider fixedCol { get; set; }
    [field: SerializeField] private bool enableColliderFix { get; set; }
    public bool EnableColliderFix { get => enableColliderFix; }

    [field: Header("--- RPG Misile Properties ---")]
    [field: SerializeField] private RPG_Missile missile { get; set; }
    public RPG_Missile Missile { get => missile; }

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private InteractionLayerMask previousLayerMask { get; set; }
    [field: SerializeField, ReadOnlyField] private InteractionLayerMask temporalLayerMask { get; set; } = 2;    // 2 = Direct Drag

    private bool forDisabling { get; set; }

    void Start() {
        ReSupply();
        previousLayerMask = myInteractable.interactionLayers;
    }

    public void ReduceAmmo() {
        ammoCount--;
        Debug.Log($"Ammo: {ammoCount}");
    }

    public void ReSupply() {
        ammoCount = maxAmmo;
    }

    public void SwapColliders() {
        if (enableColliderFix) {
            defaultCol.enabled = !defaultCol.enabled;
            fixedCol.enabled = !fixedCol.enabled;
            //Debug.Log("Collider swap");
        }
    }

    public void CancelDisabling() {
        forDisabling = false;
    }

    public void DisableIfUsed() {
        if (ammoCount < maxAmmo)
            MarkForDisabling();
    }

    public void MarkForDisabling(float time=1f) {
        myInteractable.interactionLayers = temporalLayerMask;
        forDisabling = true;
        StartCoroutine(DisableCo(time));
    }

    IEnumerator DisableCo(float time) {
        yield return new WaitForSeconds(time);
        myInteractable.interactionLayers = previousLayerMask;

        if (forDisabling)
            gameObject.SetActive(false);
    }
}
