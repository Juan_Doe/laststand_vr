using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UndergroundDisabler : MonoBehaviour {

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            CharacterController cc = other.gameObject.GetComponent<CharacterController>();
            cc.enabled = false;
            other.gameObject.transform.position = new Vector3(other.gameObject.transform.position.x, 1f, other.gameObject.transform.position.z);
            cc.enabled = true;
        }
        else
            other.gameObject.SetActive(false);
    }
}