using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.XR.Interaction.Toolkit.Filtering;

public class AmmoSocket : MonoBehaviour {

    [field: Header("--- Autoattach Properties ---")]
    [field: SerializeField, GetComponentInParent, ReadOnlyField] private Weapon weapon { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private XRSocketInteractor socket { get; set; }

    [field: Header("--- Ammo Socket Properties ---")]
    [field: SerializeField] private InputActionProperty ejectAmmo { get; set; }
    [field: SerializeField, ReadOnlyField] private bool isCharged { get; set; }
    public bool IsCharged { get => isCharged; }
    [field: SerializeField, ReadOnlyField] private XRGrabInteractable interactableAmmo { get; set; }
    [field: SerializeField, ReadOnlyField] private AmmoClip ammo { get; set; }

    //[field: SerializeField, ReadOnlyField] private int currentAmmoCount { get; set; }
    [field: SerializeField, ReadOnlyField] private bool ammoLeft { get; set; }
    public bool AmmoLeft { get => ammoLeft; }


    void Update() {

        if (isCharged && interactableAmmo != null && ammo != null) {
            if (ammo.AmmoCount > 0) {
                ammoLeft = true;
            }
            else {
                ammoLeft = false;
            }

            if (weapon.IsSelected && ejectAmmo.action.WasPerformedThisFrame()) {
                ammo.MarkForDisabling();
                RemoveClip();
            }
        }
    }

    public void Reload() {
        ejectAmmo.action.Enable();

        isCharged = true;
        interactableAmmo = (XRGrabInteractable) socket.GetOldestInteractableSelected();

        if (interactableAmmo != null)
            ammo = interactableAmmo.GetComponent<AmmoClip>();

        if (ammo != null) {
            if (ammo.EnableColliderFix) {
                ammo.SwapColliders();
            }
        }
    }

    public void RemoveClip() {
        if (ammo != null) {
            if (ammo.EnableColliderFix) {
                ammo.SwapColliders();
            }
        }

        isCharged = false;
        interactableAmmo = null;
        ammo = null;
        ammoLeft = false;
        ejectAmmo.action.Disable();
        //Debug.Log("Ammo removed from socket");
    }

    public void ReduceAmmo() {
        ammo.ReduceAmmo();
    }

    public void DispararMissile() {
        ammo.Missile.Disparar();
    }
}