﻿using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class EnemyRandomWandering : EnemyState {

    private float wanderingRange = 10f;
    private float agentDoubledHeight;

    public EnemyRandomWandering(EnemyZombie enemy, NavMeshAgent agent) : base(enemy, agent) {
        currentState = STATE.RandomWandering;

        agent.speed = enemy.MoveSpeed/* / 1.5f*/;
        agent.autoBraking = false;
        agent.isStopped = false;
        agentDoubledHeight = agent.height * 2f;
    }

    public override void Enter() {
        if (enemy.RootMotion)
            agent.updatePosition = false;
        RandomWander();

        base.Enter();
    }

    public override void Update() {
        base.Update();

        CheckersOnUpdate();

        if (agent.remainingDistance < (agent.stoppingDistance + 0.05f)) {
            CheckBeforeWander();
        }

        enemy.Anim.SetFloat("speed", agent.velocity.sqrMagnitude);

        //Debug.Log($"EnemyState: {npc.name} -> {agent.remainingDistance}");
    }

    public override void Exit() {
        agent.updatePosition = true;
        agent.ResetPath();

        base.Exit();
    }

    void CheckBeforeWander() {
        if (!OnPlayerBase()) {
            ChangeState(new EnemyMovingToPlayerBase(enemy, agent));
        }
        else
            RandomWander();
    }

    void RandomWander() {
        Vector3 randomDirection = Random.insideUnitSphere * wanderingRange;
        randomDirection += enemy.transform.position;
        randomDirection.y = 0f;
        NavMeshHit hit;

        if (NavMesh.SamplePosition(randomDirection, out hit, agentDoubledHeight, NavMesh.AllAreas)) {
            agent.SetDestination(hit.position);
        }
        else {
            CheckersOnUpdate();
            CheckBeforeWander();
        }
    }

    void CheckersOnUpdate() {
        if (CheckIfPlayerIsInRange()) {
            ChangeState(new EnemyMovingToPlayer(enemy, agent));
        }
    }
}