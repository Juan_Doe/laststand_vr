using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bootstrap : MonoBehaviour {

    [field: SerializeField, FindObjectOfType] private PoolManager poolManager { get; set; }

    private void Start() {
        // Allocate memory and create pooled GameObjects
        poolManager.InstantiatePool();

        // Get GameObject from pool 
        var cube = poolManager.Spawn("[Clip] Pistol");
        var sphere = poolManager.Spawn("[Pistol] Pistola");
    }
}