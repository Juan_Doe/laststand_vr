using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ScoreUIManager : MonoBehaviour, ISelectHandler {

    [field: Header("AutoAttach on Editor properties")]
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private ScoreManager scoreManager { get; set; }

    [field: Header("Score UI")]
    [field: SerializeField] private GameObject scoreUI { get; set; }
    [field: SerializeField] private Text scoreBoardText { get; set; }
    [field: SerializeField] private Text currentScoreText { get; set; }
    [field: SerializeField] private InputField nameInput { get; set; }
    [field: SerializeField] private Button recordButton { get; set; }
    [field: SerializeField] private Button clearButton { get; set; }
    [field: SerializeField] private GameObject keyboard { get; set; }

    private int playerScore; // You can update this value based on your game logic

    /*private void Start() {
        // Add listeners to your buttons
        recordButton.onClick.AddListener(RecordScore);
        clearButton.onClick.AddListener(ClearScores);
        UpdateScores(); // Display initial scores
    }*/

    public void UpdateScores() {
        // Display high scores!
        scoreBoardText.text = "";
        for (int i = 0; i < Leaderboard.EntryCount; ++i) {
            var entry = Leaderboard.GetEntry(i);
            string scoreStr = entry.score.ToString();
            if (scoreStr.Length > 6) {
                scoreStr = scoreStr.Substring(0, 6) + "...";
            }
            scoreBoardText.text += /*"Name: " + */entry.name + ": " + scoreStr + "\n";
        }
    }

    public void CheckInputField() {
        if (nameInput.text == "") {
            recordButton.interactable = false;
            Debug.Log("Name is empty. Please enter a name.");
            return;
        }
        recordButton.interactable = true;
    }

    public void RecordScore() {

        if (keyboard != null) {
            keyboard.SetActive(false);
        }

        if (Leaderboard.CheckScore(playerScore)) {
            Leaderboard.Record(nameInput.text, playerScore);
            // Reset for next input.
            nameInput.text = "";
            UpdateScores(); // Update score display
        }
        else {
            Debug.Log("Score is not high enough for leaderboard.");
        }
    }

    public void ClearScores() {
        Leaderboard.Clear();
        UpdateScores(); // Update score display
    }

    public void OnSelect(BaseEventData eventData) {
        if (eventData.selectedObject.CompareTag("EditorOnly")) {
            keyboard.SetActive(true);
            Debug.Log("Keyboard opened.");
        }
        else {
            keyboard.SetActive(false);
        }
    }

    public void ShowScore() {
        scoreUI.SetActive(true);
        playerScore = scoreManager.CalculateScore();
        currentScoreText.text = string.Format("{0:D10}", playerScore, 0);
    }
}