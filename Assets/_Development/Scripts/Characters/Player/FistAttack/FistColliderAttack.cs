using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FistColliderAttack : MonoBehaviour {
	
	[field: Header("Autoattach on Editor properties")]
	[field: SerializeField, FindObjectOfType, ReadOnlyField] private PlayerManager playerManager { get; set; }
	[field: SerializeField, GetComponent, ReadOnlyField] private Collider fistCollider { get; set; }

	[field: Header("Fist properties")]
	[field: SerializeField] private bool isRightFist { get; set; } = true;
	[field: SerializeField] private InputActionProperty gripActionRightHand { get; set; }
	[field: SerializeField] private InputActionProperty gripActionLeftHand { get; set; }
	[field: SerializeField] private float damage { get; set; } = 10f;
	[field: SerializeField] private float damageCooldown { get; set; } = 0.25f;
	private float damageCooldownAlt { get; set; }

	[field: SerializeField] private GameObject impactBloodVFX { get; set; }

    private void Update() {
        if (damageCooldownAlt > 0)
            damageCooldownAlt -= Time.deltaTime;

		if (gripActionRightHand.action.IsPressed() && isRightFist) {
			fistCollider.enabled = true;
		}
		else if (gripActionLeftHand.action.IsPressed() && !isRightFist) {
			fistCollider.enabled = true;
		}
		else
			fistCollider.enabled = false;
	}

    private void OnTriggerEnter(Collider other) {
        if (isRightFist)
            if (playerManager.rightHand.hasSelection)
                return;

        if (!isRightFist)
			if (playerManager.leftHand.hasSelection)
				return;

		if (other.CompareTag("Enemy")) {
			if (damageCooldownAlt > 0)
                return;
			damageCooldownAlt = damageCooldown;
			other.GetComponent<EnemyZombie>().TakeDamage(damage);
			GameObject vfx = Instantiate(impactBloodVFX, transform.position, Quaternion.identity);
			Destroy(vfx, 5f);
		}
    }

}