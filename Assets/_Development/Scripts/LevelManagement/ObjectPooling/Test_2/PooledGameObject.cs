using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class PooledGameObject : MonoBehaviour {

    [field: Header("PooledGameObject properties")]
    private IObjectPool<PooledGameObject> pool;
    private PoolManager poolManager;

    [field: SerializeField] public PoolManager.ObjectType ObjectType { get; private set; }

    [field: SerializeField, ReadOnlyField] public string Id { get; private set; }
    [field: SerializeField, ReadOnlyField] public bool IsRelease { get; private set; }

    [SerializeField, ReadOnlyField] private Vector3 startPos = Vector3.zero;
    public Vector3 StartPos {
        get {
            return startPos;
        }
    }
    [SerializeField, ReadOnlyField] private Quaternion startRot = Quaternion.identity;
    public Quaternion StartRot {
        get {
            return startRot;
        }
    }

    private bool started { get; set;}

    void OnEnable() {
        if (started) {
            transform.position = startPos;
            transform.rotation = startRot;
        }
    }

    void Start() {
        startPos = transform.position;
        startRot = transform.rotation;
        started = true;
    }

    public void Initialize(PoolManager poolMgr, string id) {
        pool = poolMgr.Pool[id];
        poolManager = poolMgr;
        Id = id;
        poolManager.OnCleanup += OnCleanup;
        poolManager.OnRelease += OnRelease;
    }

    public void SetActive(bool active) {
        // Sets gameObject ready to used
        gameObject.SetActive(active);
        IsRelease = !active;
    }

    public void OnRelease() {
        if (IsRelease) {
            return;
        }

        // Return object to pool
        pool.Release(this);
    }

    private void OnDestroy() {
        // gameObject gets destroyed unregister from pool
        if (poolManager) {
            poolManager.OnCleanup -= OnCleanup;
            poolManager.OnRelease -= OnRelease;
        }
    }

    private void OnCleanup() {
        // Called when pool manager gets cleanup
        OnRelease();
        Destroy(gameObject);
    }
}