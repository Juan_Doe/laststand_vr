using EzySlice;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRSlicer : MonoBehaviour {

    [SerializeField] private Transform startSlicePoint;
    [SerializeField] private Transform endSlicePoint;
    [SerializeField] private LayerMask sliceLayerMask;

    [SerializeField] private Material sliceSectionMaterial;

    [SerializeField] private float cutForce = 150f;

    [SerializeField] private VelocityEstimator velocityEstimator;

    private GameObject objectToSlice;
	
    void Start() {
        
    }

    void FixedUpdate() {
        bool hasHit = Physics.Linecast(startSlicePoint.position, endSlicePoint.position, out RaycastHit hit, sliceLayerMask);

        if (hasHit) {
            Slice(hit.transform.gameObject);
        }
    }

    public void Slice(GameObject target) {
        objectToSlice = target;

        Vector3 velocity = velocityEstimator.GetVelocityEstimate();
        Vector3 planeNormal = Vector3.Cross(endSlicePoint.position - startSlicePoint.position, velocity).normalized;

        SlicedHull hull = target.Slice(endSlicePoint.position, planeNormal, sliceSectionMaterial);

        if (hull != null) {
            GameObject upperHull = hull.CreateUpperHull(target/*, sliceSectionMaterial*/);
            SetupSlicedComponent(upperHull);
            GameObject lowerHull = hull.CreateLowerHull(target/*, sliceSectionMaterial*/);
            SetupSlicedComponent(lowerHull);

            Destroy(target);
        }
    }

    public void SetupSlicedComponent(GameObject slicedObject) {
        Rigidbody rb = slicedObject.AddComponent<Rigidbody>();
        MeshCollider collider = slicedObject.AddComponent<MeshCollider>();
        collider.convex = true;

        slicedObject.layer = objectToSlice.layer;

        rb.AddExplosionForce(cutForce, slicedObject.transform.position, 1);

        Destroy(slicedObject, 5f);
    }
}
