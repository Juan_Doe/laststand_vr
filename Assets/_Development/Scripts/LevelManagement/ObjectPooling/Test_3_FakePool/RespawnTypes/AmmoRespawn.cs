using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoRespawn : Respawn {

	/*[field: Header("--- Ammo respawn properties ---")]
	[field: SerializeField protected override ObjectTypeEnum objectType { get; set; } = ObjectTypeEnum.Ammo;*/

	[field: Header("Auttoattach on Editor properties")]
	[field: SerializeField, GetComponent, ReadOnlyField] private AmmoClip ammo { get; set; }

    public override void OnDisable() {
		ammo.ReSupply();

        base.OnDisable();
    }
}