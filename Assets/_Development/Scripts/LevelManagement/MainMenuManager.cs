using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using Unity.XR.CoreUtils;
using UnityEngine;

public class MainMenuManager : MonoBehaviour {

	[field: Header("AutoAttach on Editor properties")]
	[field: SerializeField, FindObjectOfType, ReadOnlyField] private PauseManager pauseManager { get; set; }
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private LevelManager levelManager { get; set; }

    [field: Header("MainMenu properties")]
    [field: SerializeField] private GameObject mainMenuCanvas { get; set; }
    [field: SerializeField] private GameObject optionsMenuCanvas { get; set; }

    public static bool isMainMenuActive { get; private set; }

    IEnumerator Start() {
        mainMenuCanvas.SetActive(true);
        isMainMenuActive = true;
        yield return new WaitForSeconds(1f);
        if (mainMenuCanvas.activeInHierarchy)
            PauseManager.SetOnPause(true);
    }

    public void MainMenuInactive() {
        isMainMenuActive = false;
    }
}