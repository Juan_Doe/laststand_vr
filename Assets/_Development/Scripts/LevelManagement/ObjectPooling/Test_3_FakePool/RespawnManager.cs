using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class RespawnManager : MonoBehaviour {

	[field: Header("--- Respawn manager properties ---")]
	[field: SerializeField] public bool respawnEnabled { get; set; } = true;

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private List<Respawn> ammoList { get; set; } = new List<Respawn>();
    [field: SerializeField, ReadOnlyField] private List<Respawn> weaponList { get; set; } = new List<Respawn>();
    [field: SerializeField, ReadOnlyField] private List<Respawn> enemyList { get; set; } = new List<Respawn>();
    public int EnemyListCount { get => enemyList.Count; }
    [field: SerializeField] private bool revalidateProperties { get; set; }

    void OnValidate() {
#if UNITY_EDITOR
        UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage /*&& prefabConnected*/) {
            ValidateAssings();
        }
#endif
    }

    void ValidateAssings() {
        // Get all the enmies in the scene in a simplified way.
        if (enemyList.Count == 0 || revalidateProperties) {
            revalidateProperties = false;
            enemyList = FindObjectsByType<Respawn>(FindObjectsInactive.Include, FindObjectsSortMode.None)
                .Where(x => x.ObjectType == Respawn.ObjectTypeEnum.Enemy).ToList();
        }
    }


    public void AddToPool(Respawn respawn) {
        switch (respawn.ObjectType) {
            case Respawn.ObjectTypeEnum.Ammo:
                ammoList.Add(respawn);
                break;

            case Respawn.ObjectTypeEnum.Weapon:
                weaponList.Add(respawn);
                break;

            case Respawn.ObjectTypeEnum.Enemy:
                enemyList.Add(respawn);
                break;
        }
    }

    public void ReactivateAmmo() {
        StartCoroutine(ReactivateAmmoCo());
    }

    private IEnumerator ReactivateAmmoCo() {
        foreach (Respawn respawn in ammoList) {
            respawn.gameObject.SetActive(true);
            yield return null;  // To distribute the load over different frames and avoid performance issues.
        }
        ammoList.Clear();
    }

    public void ReactivateWeapons() {
        StartCoroutine(ReactivateWeaponsCo());
    }

    public IEnumerator ReactivateWeaponsCo() {
        foreach (Respawn respawn in weaponList) {
            respawn.gameObject.SetActive(true);
            yield return null;  // To distribute the load over different frames and avoid performance issues.
        }
        weaponList.Clear();
    }

    public void GetEnemyFromPool() {
        if (enemyList.Count > 0) {
            EnemyRespawn enemy = (EnemyRespawn) enemyList[0];
            //Debug.Log($"<color=yellow>Spawn Enemy: {enemy}</color>");
            enemyList.RemoveAt(0);
            enemy.gameObject.SetActive(true);
            //enemy.ResetEnemy();
        }
    }


}