using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.Interaction.Toolkit;

public class HandDetector : MonoBehaviour {

	[SerializeField] private bool isRightHand;
    public bool IsRightHand {  get { return isRightHand; } }

	public void OnGrab(SelectEnterEventArgs args) {
        if (args.interactableObject.transform.CompareTag("RightHand")) {
            isRightHand = true;
        } else if (args.interactableObject.transform.CompareTag("LeftHand")) {
            isRightHand = false;
        }
    }
}
