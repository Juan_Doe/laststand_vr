using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class W_Blade : WeaponMelee {

	//[field: Header("Autoattach properties")]

	[field: Header("--- Blade Properties ---")]
    [field: SerializeField] private float continuousDamage { get; set; } = 0.4f;
    //[field: SerializeField] private bool canSlice { get; set; }

    [field: Header("Debug")]
    //[field: SerializeField, ReadOnlyField] private bool isSelected { get; set; }
    [field: SerializeField, ReadOnlyField] private EnemyZombie currentEnemy { get; set; }

    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Enemy")) {
            currentEnemy = other.GetComponent<EnemyZombie>();

            if (currentEnemy.CurrentHealth <= weaponDamage) {
                // ToDo: Slice
            }

            currentEnemy.TakeDamage(weaponDamage);
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.CompareTag("Enemy")) {
            currentEnemy?.TakeDamage(continuousDamage);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.CompareTag("Enemy")) {
            currentEnemy = null;
        }
    }
}