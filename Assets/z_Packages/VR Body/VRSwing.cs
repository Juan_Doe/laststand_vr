using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class VRSwing : MonoBehaviour {

    [field: SerializeField] private Transform startSwingHand { get; set; }
    [field: SerializeField] private float maxDistance { get; set; } = 35f;
    [field: SerializeField] private LayerMask swingLayer { get; set; }

    [field: SerializeField] private Transform predictionPoint { get; set; }
    [field: SerializeField] private Transform startSwingVisual { get; set; }
    [field: SerializeField] private Transform cam { get; set; }
    [field: SerializeField] private float nitroBoost { get; set; } = 150f;

    private Vector3 swingPoint { get; set; }

    [field: SerializeField] private InputActionProperty swingAction { get; set; }
    [field: SerializeField] private InputActionProperty nitroAction { get; set; }

    [field: SerializeField] private Rigidbody playerRb { get; set; }
    private SpringJoint joint { get; set; }

    [field: SerializeField] private LineRenderer lineRenderer { get; set; }

    [field: SerializeField] private bool hasHit { get; set; }


    void Update() {
        if (joint) {
            predictionPoint.gameObject.SetActive(false);
            return;
        }
        GetSwingPoint();

        if (swingAction.action.WasPressedThisFrame() && hasHit) {
            StartSwing();
        }
        else if (swingAction.action.WasReleasedThisFrame()) {
            StopSwing();
        }

        DrawRope();
        NitroBoost();
    }

    void StartSwing() {
        joint = playerRb.gameObject.AddComponent<SpringJoint>();
        joint.autoConfigureConnectedAnchor = false;
        joint.connectedAnchor = swingPoint;

        float distanceFromPoint = Vector3.Distance(playerRb.transform.position, swingPoint);
        joint.maxDistance = distanceFromPoint;

        joint.spring = 4.5f;
        joint.damper = 7f;
        joint.massScale = 4.5f;
    }

    void StopSwing() {
        Destroy(joint);
    }

    void GetSwingPoint() {
        RaycastHit hit;

        hasHit = Physics.Raycast(startSwingHand.position, startSwingHand.forward, out hit, maxDistance, swingLayer);

        if (hasHit) {
            Debug.DrawRay(startSwingHand.position, startSwingHand.forward * hit.distance, Color.yellow);

            swingPoint = hit.point;
            predictionPoint.gameObject.SetActive(true);
            predictionPoint.position = swingPoint;
        }
        else {
            //Debug.DrawRay(startSwingHand.position, startSwingHand.forward * maxDistance, Color.white);
            predictionPoint.gameObject.SetActive(false);
        }
    }

    void DrawRope() {
        if (!joint) {
            lineRenderer.enabled = false;
        }
        else {
            lineRenderer.enabled = true;
            lineRenderer.positionCount = 2;
            lineRenderer.SetPosition(0, startSwingVisual.position);
            lineRenderer.SetPosition(1, swingPoint);
        }
    }

    void NitroBoost() {
        if (!joint)
            return;

        if (nitroAction.action.IsPressed()) {
            playerRb.AddForce(cam.forward * nitroBoost * Time.deltaTime);
        }
    }
}
