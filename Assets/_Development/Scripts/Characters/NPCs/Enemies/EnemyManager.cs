using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

public class EnemyManager : MonoBehaviour {

    [field: Header("Autoattach properties")]
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private PlayerManager player { get; set; }
    public PlayerManager Player { get => player; }
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private LevelManager levelManager { get; set; }
    [field: SerializeField, FindObjectOfType, ReadOnlyField] private RespawnManager respawnManager { get; set; }
    [field: SerializeField, ReadOnlyField] private List<Transform> allSpawnPoints { get; set; } = new List<Transform>();

    [field: SerializeField, ReadOnlyField] private Transform[] spawnPointsZone1 { get; set; }
    [field: SerializeField, ReadOnlyField] private Transform[] spawnPointsZone2 { get; set; }
    [field: SerializeField, ReadOnlyField] private Transform[] spawnPointsZone3 { get; set; }
    [field: SerializeField, ReadOnlyField] private Transform[] spawnPointsZone4 { get; set; }
    [field: SerializeField] private bool revalidateProperties { get; set; }

    public enum WaveStates {
        Stopped,
        InProgress,
        OnCooldown
    }

    [field: Header("Enemy respawn settings")]
    [field: SerializeField] private float enemyRespawnCooldown { get; set; } = 2f;
    private float enemyRespawnTimer { get; set; } = 0f;

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private WaveStates waveState { get; set; } = WaveStates.Stopped;
    public WaveStates WaveState { get => waveState; }
    private bool zone2Open { get; set; }
    private bool zone3Open { get; set; }
    private bool zone4Open { get; set; }

    void OnValidate() {
#if UNITY_EDITOR
        UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage && prefabConnected) {
            if (revalidateProperties)
                ValidateAssings();
        }
#endif
    }

    void ValidateAssings() {
        // Get all the spawnPoints in the scene in a simplified way.
        if (spawnPointsZone1 == null || spawnPointsZone1.Length == 0 || revalidateProperties) {
            //revalidateProperties = false;
            spawnPointsZone1 = GameObject.FindGameObjectsWithTag("Respawn_1").Select(x => x.transform).ToArray();
        }
        if (spawnPointsZone2 == null || spawnPointsZone2.Length == 0 || revalidateProperties) {
            //revalidateProperties = false;
            spawnPointsZone2 = GameObject.FindGameObjectsWithTag("Respawn_2").Select(x => x.transform).ToArray();
        }
        if (spawnPointsZone3 == null || spawnPointsZone3.Length == 0 || revalidateProperties) {
            //revalidateProperties = false;
            spawnPointsZone3 = GameObject.FindGameObjectsWithTag("Respawn_3").Select(x => x.transform).ToArray();
        }
        if (spawnPointsZone4 == null || spawnPointsZone4.Length == 0 || revalidateProperties) {
            //revalidateProperties = false;
            spawnPointsZone4 = GameObject.FindGameObjectsWithTag("Respawn_4").Select(x => x.transform).ToArray();
        }

        if (allSpawnPoints == null || allSpawnPoints.Count == 0 || revalidateProperties) {
            //revalidateProperties = false;
            allSpawnPoints = spawnPointsZone1.ToList();
        }
        revalidateProperties = false;
    }

    /*private bool playerDetected { get; set; }
    public bool PlayerDetected { get => playerDetected; set => playerDetected = value; }*/

    void Update() {
        if (waveState == WaveStates.InProgress) {
            // Spawn enemies
            if (respawnManager.EnemyListCount > 0) {
                // Spawn enemy
                if (enemyRespawnTimer <= 0) {
                    respawnManager.GetEnemyFromPool();

                    enemyRespawnTimer = enemyRespawnCooldown;
                }
                else {
                    enemyRespawnTimer -= Time.deltaTime;
                }
            }
        }
        else if (waveState == WaveStates.OnCooldown) {
            enemyRespawnTimer = 0f;
        }
        else if (waveState == WaveStates.Stopped) {
            enemyRespawnTimer = 0f;
        }

        //Debug.Log($"Wave state: {waveState} -> timer: {enemyRespawnTimer}/{enemyRespawnCooldown}");
    }

    public void MoveEnemyToRandomSpawn(Transform enemy) {
        switch (levelManager.CurrentWave) {
            case 4:
                if (!zone2Open) {
                    zone2Open = true;
                    allSpawnPoints.AddRange(spawnPointsZone2.ToList());
                }
                break;
            case 7:
                if (!zone3Open) {
                    zone3Open = true;
                    allSpawnPoints.AddRange(spawnPointsZone3.ToList());
                }
                break;
            case 9:
                if (!zone4Open) {
                    zone4Open = true;
                    allSpawnPoints.AddRange(spawnPointsZone4.ToList());
                }
                break;
        }

        //int randomSpawnIndex = Random.Range(0, allSpawnPoints.Count);
        int randomSpawnIndex = GenerateRandomNumber(0, allSpawnPoints.Count);

        /*enemy.position = spawnPoints[randomSpawnIndex].position;
        enemy.rotation = spawnPoints[randomSpawnIndex].rotation;*/

        enemy.position = NavMesh.SamplePosition(allSpawnPoints[randomSpawnIndex].position, out NavMeshHit hit,
            1f, NavMesh.AllAreas) ? hit.position : allSpawnPoints[randomSpawnIndex].position;
    }

    public void StartEnemyRespawn() {
        waveState = WaveStates.InProgress;
    }

    public void StopEnemyRespawn() {
        waveState = WaveStates.OnCooldown;
    }

    public void StopWaves() {
        waveState = WaveStates.Stopped;
        respawnManager.respawnEnabled = false;
    }

    int GenerateRandomNumber(int minInclusive, int maxExclusive) {
        int seed = System.DateTime.Now.Millisecond;
        Random.InitState(seed);
        return Random.Range(minInclusive, maxExclusive);
    }
}