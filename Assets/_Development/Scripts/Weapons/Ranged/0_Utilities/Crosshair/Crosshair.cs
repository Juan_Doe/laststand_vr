using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crosshair : MonoBehaviour {

	[field: Header("--- Crosshair Properties ---")]
	[field: SerializeField, GetComponent, ReadOnlyField] private SpriteRenderer spriteRenderer { get; set; }
	//public SpriteRenderer SpriteRenderer { get => spriteRenderer; }

    void OnEnable() {
        if (spriteRenderer != null && !spriteRenderer.enabled) {
            spriteRenderer.enabled = true;
        }
    }
}