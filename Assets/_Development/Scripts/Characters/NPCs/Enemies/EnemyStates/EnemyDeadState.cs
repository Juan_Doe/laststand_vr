﻿using UnityEngine;
using UnityEngine.AI;

public class EnemyDeadState : EnemyState {

    private float waitingTime_1 = 5f;
    private float waitingTime_2 = 5f;
    
    /*private float deadEffectSpeed = 1.25f;
    private float deadStopPosY = -5f;*/

    public EnemyDeadState(EnemyZombie enemy, NavMeshAgent agent) : base(enemy, agent) {
        currentState = STATE.Dead;

        agent.isStopped = true;
    }

    public override void Enter() {
        base.Enter();

        enemy.gameObject.layer = 13;    // EnemyBody
        enemy.Anim.SetBool("IsDead", true);
        inDeadState = true;
        agent.enabled = false;
        //enemy.Col.enabled = false;

        // Simulate explosion effect
        if (enemy.AffectedByExplosion) {
            enemy.AffectedByExplosion = false;
            enemy.Rb.drag = 6f;
            enemy.Rb.constraints = RigidbodyConstraints.None;
            enemy.Rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            agent.enabled = false;
            enemy.Rb.AddForce(Vector3.up * 25f, ForceMode.Impulse);
            enemy.Rb.AddTorque(new Vector3(Random.Range(1, 6), Random.Range(1, 6), Random.Range(1, 6)) * 2f, ForceMode.Impulse);
        }
    }

    public override void Update() {
        base.Update();


        if (enemy.Anim.GetCurrentAnimatorStateInfo(0).IsTag("Dead")) {
            if (waitingTime_1 <= 0f) {
                enemy.Rb.isKinematic = true;
                // Make enemy disappear
                if (waitingTime_2 > 0f) {
                    // With scale reduction
                    //enemy.transform.localScale = Vector3.MoveTowards(enemy.transform.localScale, Vector3.zero, Time.deltaTime * deadEffectSpeed * 1.5f);
                    // And slowly push it down
                    enemy.Rb.isKinematic = true;
                    agent.enabled = false;

                    /*enemy.transform.position = Vector3.MoveTowards(enemy.transform.position,
                        new Vector3(enemy.transform.position.x, deadStopPosY, enemy.transform.position.z),
                        Time.deltaTime * deadEffectSpeed);*/

                    waitingTime_2 -= Time.deltaTime;
                }
                else if (enemy.gameObject.activeInHierarchy) {
                    Reset();
                    enemy.gameObject.SetActive(false);
                }
            }
            else {
                waitingTime_1 -= Time.deltaTime;
            }
        }
    }

    public override void Exit() {
        Reset();

        base.Exit();
    }

    void Reset() {
        enemy.IsDead = false;
        inDeadState = false;
        enemy.Anim.SetBool("IsDead", false);
        waitingTime_1 = 5f;
        waitingTime_2 = 5f;
        enemy.Rb.constraints = RigidbodyConstraints.FreezeAll;
        enemy.gameObject.layer = 10;    // Enemy
        agent.enabled = true;
        agent.ResetPath();
        enemy.Col.enabled = true;

        enemy.Rb.isKinematic = false;

        enemy.transform.localScale = Vector3.one;

        enemy.Rb.drag = 0f;
        enemy.Rb.collisionDetectionMode = CollisionDetectionMode.Discrete;

    }
}