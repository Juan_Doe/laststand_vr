using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class W_RPG : WeaponRanged {

    [field: Header("__ Autoattach Properties __")]
    [field: SerializeField, GetComponentInChildren, ReadOnlyField] private Crosshair crosshair { get; set; }
    [field: SerializeField, GetComponentInChildren, ReadOnlyField] private AmmoSocket ammoClip { get; set; }

    [field: Header("--- RPG Properties ---")]
    [field: SerializeField] private RPG_Missile misilePrefab { get; set; }
    [field: SerializeField] private Transform misileSpawnPoint { get; set; }

    [field: Header("--- Crosshair Properties ---")]
    [field: SerializeField] private LayerMask targetMask { get; set; } = 1 << 0 | 1 << 3 | 1 << 10 | 1 << 12;
    [field: SerializeField, Range(0f, 0.5f)] private float pointOffset { get; set; } = 0.003f;

    [field: Header("Debug")]
    [field: SerializeField] private bool unlimitedAmmo { get; set; }
    //[field: SerializeField, ReadOnlyField] private bool isSelected { get; set; }

    private RaycastHit hit;

    public void Disparar() {
        // Fire rate
        if (Time.time < nextTimeToFire)
            return;

        nextTimeToFire = Time.time + weaponFireRate;

        // Ammo
        if (!unlimitedAmmo) {
            if (!ammoClip.IsCharged || !ammoClip.AmmoLeft)
                return;

            ammoClip.ReduceAmmo();
        }

        // Muzzle flash
        if (muzzleFlash != null)
            muzzleFlash.Play();

        /*RPG_Misile spawnedMisile = Instantiate(misilePrefab, misileSpawnPoint.position, misileSpawnPoint.rotation);

        spawnedMisile.AddForce(misileSpawnPoint.forward * bulletSpeed, ForceMode.Impulse);

        Destroy(spawnedMisile, 3f);*/

        // Misile
        ammoClip.DispararMissile();
    }

    void FixedUpdate() {
        if (isSelected && (ammoClip.IsCharged && ammoClip.AmmoLeft) || unlimitedAmmo) {
            UpdateCrosshairPosition();
        }
        else {
            DisableCrosshair();
        }
    }

    void UpdateCrosshairPosition() {
        if (Physics.Raycast(misileSpawnPoint.position, misileSpawnPoint.forward, out hit, weaponRange, targetMask)) {
            // Si el raycast golpea un objeto, muestra el crosshair
            crosshair.gameObject.SetActive(true);
            //crosshair.transform.position = hit.point;
            crosshair.transform.position = hit.point + hit.normal * pointOffset;
            crosshair.transform.rotation = Quaternion.FromToRotation(Vector3.forward, hit.normal);
            //crosshair.transform.LookAt(Camera.main.transform);
        }
        else {
            // Si el raycast no golpea un objeto, oculta el crosshair
            DisableCrosshair();
        }
    }

    void DisableCrosshair() {
        crosshair.transform.rotation = Quaternion.identity;
        crosshair.gameObject.SetActive(false);
    }
}