using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class W_Shuriken : WeaponThrowable {

    [field: Header("Autoattach properties")]
    [field: SerializeField, GetComponent, ReadOnlyField] private Rigidbody rb { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private Collider col { get; set; }

    [field: Header("--- Shuriken Properties ---")]
    [field: SerializeField] private float magnitudeToCountAsThrown { get; set; } = 5f;
    [field: SerializeField, Tooltip("Cancel when grabbed again")] private float lifeTimeAfterBeingUsed { get; set; } = 10f;

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private bool thrown { get; set; }
    [field: SerializeField, ReadOnlyField] private bool stucked { get; set; }

    private bool checkThrow { get; set; }
    private Coroutine detectThrowAndThrowCo { get; set; }
    private Coroutine disableAfterSecondsCo { get; set; }


    //private bool test { get; set; }

    // Called on LastSelected.
    public void CheckThrow() {
        if (checkThrow)
            return;

        detectThrowAndThrowCo = StartCoroutine(DetectThrowAndThrowCo());
    }

    private void Throw() {
        if (thrown)
            return;

        //test = true;
        //Debug.Log($"<color=yellow>Throwing {weaponName} | Velocity -> {rb.velocity.sqrMagnitude}</color>");

        /*if (rb.velocity.sqrMagnitude < magnitudeToCountAsThrown)
            return;*/

        thrown = true;
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
        rb.isKinematic = false;
        rb.useGravity = true;
        // Has impulse and rotation
        rb.AddForce(transform.forward * weaponThrowForce, ForceMode.Impulse);
        rb.AddTorque(transform.forward * weaponThrowForce, ForceMode.Impulse);
        col.enabled = false;
    }

    /*private void Update() {
        if (test) {
            Debug.Log($"<color=yellow>Throwing {weaponName} | Velocity -> {rb.velocity.sqrMagnitude}</color>");
        }
    }*/

    private void OnTriggerEnter(Collider other) {
        if (stucked)
            return;

        if (thrown) {
            if (other.CompareTag("Untagged")) {
                StickOnPoint(other.transform);
            }

            if (other.CompareTag("Enemy")) {
                StickOnPoint(other.transform);
                other.GetComponent<EnemyZombie>().TakeDamage(weaponDamage);
            }
        }
    }

    private void StickOnPoint(Transform parent) {
        stucked = true;
        rb.isKinematic = true;
        rb.useGravity = false;
        rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
        transform.SetParent(parent);
        disableAfterSecondsCo = StartCoroutine(DisableAfterSecondsCo());
    }

    IEnumerator DetectThrowAndThrowCo() {
        checkThrow = true;
        StartCoroutine(StopCheckingThrowCo());
        while (checkThrow) {
            if (rb.velocity.sqrMagnitude > magnitudeToCountAsThrown) {
                checkThrow = false;
                Throw();
            }
            yield return null;
        }
    }

    IEnumerator StopCheckingThrowCo() {
        yield return new WaitForSeconds(0.15f);
        checkThrow = false;
        if (detectThrowAndThrowCo != null)
            StopCoroutine(detectThrowAndThrowCo);
    }

    IEnumerator DisableAfterSecondsCo() {
        yield return new WaitForSeconds(lifeTimeAfterBeingUsed);
        gameObject.SetActive(false);
    }

    public void CancelDestruction() {
        if (disableAfterSecondsCo != null)
            StopCoroutine(disableAfterSecondsCo);
    }

    void ResetObject() {
        StopAllCoroutines();

        thrown = false;
        stucked = false;
        checkThrow = false;

        rb.useGravity = true;
        rb.isKinematic = false;
        rb.collisionDetectionMode = CollisionDetectionMode.Discrete;
        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        col.enabled = true;
    }

    void OnDisable() {
        ResetObject();
    }
}