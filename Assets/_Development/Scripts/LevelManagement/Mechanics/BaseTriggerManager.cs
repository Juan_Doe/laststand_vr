using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseTriggerManager : MonoBehaviour {
	
	[field: Header("Autoattach on Editor properties")]
	[field: SerializeField, FindObjectOfType, ReadOnlyField] private PlayerManager playerManager { get; set; }

    private void OnTriggerEnter(Collider other) {
        if (playerManager != null) {
            if (other.CompareTag("Player")) {
                playerManager.DeactivatePollutionEffect();
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if (playerManager != null) {
            if (other.CompareTag("Player")) {
                playerManager.ActivatePollutionEffect();
            }
        }
    }
}