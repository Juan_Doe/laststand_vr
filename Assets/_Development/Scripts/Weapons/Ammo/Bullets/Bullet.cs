using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Projectile {

    [field: Header("__ Autottach properties __")]
    [field: SerializeField, GetComponent, ReadOnlyField] protected Rigidbody rb { get; set; }

    //[field: Header("--- Bullet Properties ---")]
	
    void Start() {
        rb.AddForce(transform.forward * projectileData.projectileSpeed, ForceMode.Impulse);
        Destroy(gameObject, projectileData.projectileLifetime);
    }

    /*private void OnCollisionEnter(Collision collision) {
        if (collision.gameObject.CompareTag("Enemy")) {
            collision.gameObject.GetComponent<EnemyZombie>().TakeDamage(projectileDamage);
        }
        Destroy(gameObject);
    }*/

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Enemy")) {
            other.gameObject.GetComponent<EnemyZombie>().TakeDamage(projectileData.projectileDamage);
        }
        Destroy(gameObject);
    }
}