using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour {

    [field: Header("General pause properties")]
    [field: SerializeField] private GameObject _handPauseMenu { get; set; }
    [field: SerializeField] private GameObject _settingsMenu { get; set; }
    [field: SerializeField, ReadOnlyField] public static bool onPause { get; private set; } = false;

    [field: SerializeField] private GameObject[] objectsToDisable { get; set; }

    /*[field: Header("Setting menu")]
    [field: SerializeField] private Slider _volumeSlider { get; set; }
    [field: SerializeField] private AudioMixer _audioMixer { get; set; }
    [field: SerializeField] private Slider _brightnessSlider { get; set; }
    [field: SerializeField] private Light directionalLight { get; set; }

    const string MIXER_MASTER = "MasterVol";*/

    public void Pause() {
        if (!onPause) {
            onPause = true;
            Time.timeScale = 0;
            _handPauseMenu.SetActive(true);
            UIUtility.EnableDisableObjects(false, objectsToDisable);
        }
        else {
            onPause = false;
            EnableDisableObjects(false);
            UIUtility.EnableDisableObjects(true, objectsToDisable);
            Time.timeScale = 1;
        }
    }

    private void EnableDisableObjects(bool active = true) {
        _handPauseMenu.SetActive(active);
        //_settingsMenu.SetActive(active);
    }

    /// <summary>
    /// Used for scripts with UI elements like pause menu for tell to other scripts that the game is paused.
    /// </summary>
    public static void SetOnPause(bool value) {
        onPause = value;
        if (value)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;
    }

    public void Quit() {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }

    public void Restart() {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}