using Nrjwolf.Tools.AttachAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Manage the score of a level.
/// </summary>

public class ScoreManager : MonoBehaviour {

    [field: Header("AutoAttach on Editor properties")]
    //[field: SerializeField, FindObjectOfType, ReadOnlyField] private ScoreUIManager scoreUIManager { get; set; }
    [field: SerializeField, GetComponent, ReadOnlyField] private LevelTimer _levelTimer { get; set; }
    [field: SerializeField] private bool revalidateProperties { get; set; }

    [field: Header("Score properties")]
    [field: SerializeField, ReadOnlyField] private int _maxEnemies { get; set; }

    // Pesos en la puntuación
    [field: SerializeField, Range(0, 100)] private int _timeWeight { get; set; } = 20;
    //[field: SerializeField] private int ObjectivesWeight { get; set; }
    [field: SerializeField, Range(0, 100)] private int _enemiesWeight { get; set; } = 80;

    [field: Header("Debug")]
    [field: SerializeField, ReadOnlyField] private int _enemiesDefeatedDebug { get; set; }

    private static int _enemiesDefeated { get; set; }
    public static int EnemiesDefeated {
        get { return _enemiesDefeated; }
        set { _enemiesDefeated = value; }
    }
    [field: SerializeField, ReadOnlyField] private int _inspectorScore { get; set; } // Puntuación que se muestra en el inspector.
    private static int _score { get; set; }
    public static int Score {
        get { return _score; }
        set { _score = value; }
    }

    private int _timeScore { get; set; }

    private void OnValidate() {
#if UNITY_EDITOR
        UnityEditor.SceneManagement.PrefabStage prefabStage = UnityEditor.SceneManagement.PrefabStageUtility.GetCurrentPrefabStage();
        bool isValidPrefabStage = prefabStage != null && prefabStage.stageHandle.IsValid();
        bool prefabConnected = PrefabUtility.GetPrefabInstanceStatus(this.gameObject) == PrefabInstanceStatus.Connected;
        if (!isValidPrefabStage/* && prefabConnected*/) {
            //Variables que solo se verificaran cuando estan en una escena
            if (revalidateProperties) {
                Validate();
            }
        }
#endif
    }

    private void Validate() {
        if (_maxEnemies <= 0 || revalidateProperties) {
            _maxEnemies = FindObjectsByType<EnemyZombie>(FindObjectsInactive.Include, FindObjectsSortMode.None).Length;
        }
        revalidateProperties = false;
    }

    private void Start() {
        _enemiesDefeated = _enemiesDefeatedDebug = 0;
        //yield return null;
    }

    public int CalculateScore() {
        // Puntuación del tiempo (Android)
        //_timeScore = (int)(_maxScore * _timeWeight * (_maxTime - _levelTimer.TimeElapsed) / _maxTime);

        // Puntuación del tiempo (VR)
        _timeScore = (int)(_levelTimer.TimeElapsed * _timeWeight);

        // Puntuación de los enemigos neutralizados (Android)
        //int enemiesScore = _maxScore * _enemiesWeight * _enemiesDefeated / _maxEnemies;

        // Puntuación de los enemigos eliminados (VR)
        int enemiesScore = _enemiesDefeated * _enemiesWeight;

        _enemiesDefeatedDebug = _enemiesDefeated;

        return _inspectorScore = _score = _timeScore + enemiesScore;
    }

    public void AddEnemyDefeated() {
        _enemiesDefeated++;
        _enemiesDefeatedDebug = _enemiesDefeated;
    }

    public void SaveData() {
        //int sceneIndex = LoadScene.CurrentIndexScene();
        int sceneIndex = 0;
        int sceneScore = CalculateScore();

        // Guardar la puntuación total de la escena en el playerPref restándole la puntuación anterior para actualizar adecuadamente el valor.
        PlayerPrefs.SetInt("TotalScore", PlayerPrefs.GetInt("TotalScore", 0) - PlayerPrefs.GetInt($"Level_{sceneIndex}", 0) + sceneScore);

        // Guardar la puntuación de la escena en el playerPref.
        int currentScore = CalculateScore();

        if (currentScore > PlayerPrefs.GetInt($"Level_{sceneIndex}", 0)) {
            PlayerPrefs.SetInt($"Level_{sceneIndex}", currentScore);
            PlayerPrefs.SetInt($"Level_{sceneIndex}_TimeInt", _timeScore);
        }
    }
}
